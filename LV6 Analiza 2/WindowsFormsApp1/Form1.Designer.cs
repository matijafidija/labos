﻿namespace WindowsFormsApp1
{
    partial class tTries
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bA = new System.Windows.Forms.Button();
            this.bB = new System.Windows.Forms.Button();
            this.bC = new System.Windows.Forms.Button();
            this.bD = new System.Windows.Forms.Button();
            this.bS = new System.Windows.Forms.Button();
            this.bR = new System.Windows.Forms.Button();
            this.bQ = new System.Windows.Forms.Button();
            this.bP = new System.Windows.Forms.Button();
            this.bW = new System.Windows.Forms.Button();
            this.bV = new System.Windows.Forms.Button();
            this.bU = new System.Windows.Forms.Button();
            this.bT = new System.Windows.Forms.Button();
            this.bH = new System.Windows.Forms.Button();
            this.bG = new System.Windows.Forms.Button();
            this.bF = new System.Windows.Forms.Button();
            this.bE = new System.Windows.Forms.Button();
            this.bZ = new System.Windows.Forms.Button();
            this.bY = new System.Windows.Forms.Button();
            this.bX = new System.Windows.Forms.Button();
            this.bL = new System.Windows.Forms.Button();
            this.bK = new System.Windows.Forms.Button();
            this.bJ = new System.Windows.Forms.Button();
            this.bI = new System.Windows.Forms.Button();
            this.bO = new System.Windows.Forms.Button();
            this.bN = new System.Windows.Forms.Button();
            this.bM = new System.Windows.Forms.Button();
            this.bExit = new System.Windows.Forms.Button();
            this.bNewGame = new System.Windows.Forms.Button();
            this.lLetter = new System.Windows.Forms.Label();
            this.lblNoOTries = new System.Windows.Forms.Label();
            this.tbNoOTries = new System.Windows.Forms.TextBox();
            this.bReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // bA
            // 
            this.bA.Enabled = false;
            this.bA.Location = new System.Drawing.Point(284, 353);
            this.bA.Name = "bA";
            this.bA.Size = new System.Drawing.Size(35, 29);
            this.bA.TabIndex = 0;
            this.bA.Text = "A";
            this.bA.UseVisualStyleBackColor = true;
            this.bA.Click += new System.EventHandler(this.bA_Click);
            // 
            // bB
            // 
            this.bB.Enabled = false;
            this.bB.Location = new System.Drawing.Point(547, 388);
            this.bB.Name = "bB";
            this.bB.Size = new System.Drawing.Size(35, 29);
            this.bB.TabIndex = 1;
            this.bB.Text = "B";
            this.bB.UseVisualStyleBackColor = true;
            this.bB.Click += new System.EventHandler(this.bB_Click);
            // 
            // bC
            // 
            this.bC.Enabled = false;
            this.bC.Location = new System.Drawing.Point(433, 388);
            this.bC.Name = "bC";
            this.bC.Size = new System.Drawing.Size(35, 29);
            this.bC.TabIndex = 2;
            this.bC.Text = "C";
            this.bC.UseVisualStyleBackColor = true;
            this.bC.Click += new System.EventHandler(this.bC_Click);
            // 
            // bD
            // 
            this.bD.Enabled = false;
            this.bD.Location = new System.Drawing.Point(398, 353);
            this.bD.Name = "bD";
            this.bD.Size = new System.Drawing.Size(35, 29);
            this.bD.TabIndex = 3;
            this.bD.Text = "D";
            this.bD.UseVisualStyleBackColor = true;
            this.bD.Click += new System.EventHandler(this.bD_Click);
            // 
            // bS
            // 
            this.bS.Enabled = false;
            this.bS.Location = new System.Drawing.Point(341, 353);
            this.bS.Name = "bS";
            this.bS.Size = new System.Drawing.Size(35, 29);
            this.bS.TabIndex = 7;
            this.bS.Text = "S";
            this.bS.UseVisualStyleBackColor = true;
            this.bS.Click += new System.EventHandler(this.bS_Click);
            // 
            // bR
            // 
            this.bR.Enabled = false;
            this.bR.Location = new System.Drawing.Point(433, 318);
            this.bR.Name = "bR";
            this.bR.Size = new System.Drawing.Size(35, 29);
            this.bR.TabIndex = 6;
            this.bR.Text = "R";
            this.bR.UseVisualStyleBackColor = true;
            this.bR.Click += new System.EventHandler(this.bR_Click);
            // 
            // bQ
            // 
            this.bQ.Enabled = false;
            this.bQ.Location = new System.Drawing.Point(262, 318);
            this.bQ.Name = "bQ";
            this.bQ.Size = new System.Drawing.Size(35, 29);
            this.bQ.TabIndex = 5;
            this.bQ.Text = "Q";
            this.bQ.UseVisualStyleBackColor = true;
            this.bQ.Click += new System.EventHandler(this.bQ_Click);
            // 
            // bP
            // 
            this.bP.Enabled = false;
            this.bP.Location = new System.Drawing.Point(775, 318);
            this.bP.Name = "bP";
            this.bP.Size = new System.Drawing.Size(35, 29);
            this.bP.TabIndex = 4;
            this.bP.Text = "P";
            this.bP.UseVisualStyleBackColor = true;
            this.bP.Click += new System.EventHandler(this.bP_Click);
            // 
            // bW
            // 
            this.bW.Enabled = false;
            this.bW.Location = new System.Drawing.Point(319, 318);
            this.bW.Name = "bW";
            this.bW.Size = new System.Drawing.Size(35, 29);
            this.bW.TabIndex = 15;
            this.bW.Text = "W";
            this.bW.UseVisualStyleBackColor = true;
            this.bW.Click += new System.EventHandler(this.bW_Click);
            // 
            // bV
            // 
            this.bV.Enabled = false;
            this.bV.Location = new System.Drawing.Point(490, 388);
            this.bV.Name = "bV";
            this.bV.Size = new System.Drawing.Size(35, 29);
            this.bV.TabIndex = 14;
            this.bV.Text = "V";
            this.bV.UseVisualStyleBackColor = true;
            this.bV.Click += new System.EventHandler(this.bV_Click);
            // 
            // bU
            // 
            this.bU.Enabled = false;
            this.bU.Location = new System.Drawing.Point(604, 318);
            this.bU.Name = "bU";
            this.bU.Size = new System.Drawing.Size(35, 29);
            this.bU.TabIndex = 13;
            this.bU.Text = "U";
            this.bU.UseVisualStyleBackColor = true;
            this.bU.Click += new System.EventHandler(this.bU_Click);
            // 
            // bT
            // 
            this.bT.Enabled = false;
            this.bT.Location = new System.Drawing.Point(490, 318);
            this.bT.Name = "bT";
            this.bT.Size = new System.Drawing.Size(35, 29);
            this.bT.TabIndex = 12;
            this.bT.Text = "T";
            this.bT.UseVisualStyleBackColor = true;
            this.bT.Click += new System.EventHandler(this.bT_Click);
            // 
            // bH
            // 
            this.bH.Enabled = false;
            this.bH.Location = new System.Drawing.Point(570, 353);
            this.bH.Name = "bH";
            this.bH.Size = new System.Drawing.Size(35, 29);
            this.bH.TabIndex = 11;
            this.bH.Text = "H";
            this.bH.UseVisualStyleBackColor = true;
            this.bH.Click += new System.EventHandler(this.bH_Click);
            // 
            // bG
            // 
            this.bG.Enabled = false;
            this.bG.Location = new System.Drawing.Point(512, 353);
            this.bG.Name = "bG";
            this.bG.Size = new System.Drawing.Size(35, 29);
            this.bG.TabIndex = 10;
            this.bG.Text = "G";
            this.bG.UseVisualStyleBackColor = true;
            this.bG.Click += new System.EventHandler(this.bG_Click);
            // 
            // bF
            // 
            this.bF.Enabled = false;
            this.bF.Location = new System.Drawing.Point(455, 353);
            this.bF.Name = "bF";
            this.bF.Size = new System.Drawing.Size(35, 29);
            this.bF.TabIndex = 9;
            this.bF.Text = "F";
            this.bF.UseVisualStyleBackColor = true;
            this.bF.Click += new System.EventHandler(this.bF_Click);
            // 
            // bE
            // 
            this.bE.Enabled = false;
            this.bE.Location = new System.Drawing.Point(376, 318);
            this.bE.Name = "bE";
            this.bE.Size = new System.Drawing.Size(35, 29);
            this.bE.TabIndex = 8;
            this.bE.Text = "E";
            this.bE.UseVisualStyleBackColor = true;
            this.bE.Click += new System.EventHandler(this.bE_Click);
            // 
            // bZ
            // 
            this.bZ.Enabled = false;
            this.bZ.Location = new System.Drawing.Point(547, 318);
            this.bZ.Name = "bZ";
            this.bZ.Size = new System.Drawing.Size(35, 29);
            this.bZ.TabIndex = 22;
            this.bZ.Text = "Z";
            this.bZ.UseVisualStyleBackColor = true;
            this.bZ.Click += new System.EventHandler(this.bZ_Click);
            // 
            // bY
            // 
            this.bY.Enabled = false;
            this.bY.Location = new System.Drawing.Point(319, 388);
            this.bY.Name = "bY";
            this.bY.Size = new System.Drawing.Size(35, 29);
            this.bY.TabIndex = 21;
            this.bY.Text = "Y";
            this.bY.UseVisualStyleBackColor = true;
            this.bY.Click += new System.EventHandler(this.bY_Click);
            // 
            // bX
            // 
            this.bX.Enabled = false;
            this.bX.Location = new System.Drawing.Point(376, 388);
            this.bX.Name = "bX";
            this.bX.Size = new System.Drawing.Size(35, 29);
            this.bX.TabIndex = 20;
            this.bX.Text = "X";
            this.bX.UseVisualStyleBackColor = true;
            this.bX.Click += new System.EventHandler(this.bX_Click);
            // 
            // bL
            // 
            this.bL.Enabled = false;
            this.bL.Location = new System.Drawing.Point(743, 353);
            this.bL.Name = "bL";
            this.bL.Size = new System.Drawing.Size(35, 29);
            this.bL.TabIndex = 19;
            this.bL.Text = "L";
            this.bL.UseVisualStyleBackColor = true;
            this.bL.Click += new System.EventHandler(this.bL_Click);
            // 
            // bK
            // 
            this.bK.Enabled = false;
            this.bK.Location = new System.Drawing.Point(686, 353);
            this.bK.Name = "bK";
            this.bK.Size = new System.Drawing.Size(35, 29);
            this.bK.TabIndex = 18;
            this.bK.Text = "K";
            this.bK.UseVisualStyleBackColor = true;
            this.bK.Click += new System.EventHandler(this.bK_Click);
            // 
            // bJ
            // 
            this.bJ.Enabled = false;
            this.bJ.Location = new System.Drawing.Point(627, 353);
            this.bJ.Name = "bJ";
            this.bJ.Size = new System.Drawing.Size(35, 29);
            this.bJ.TabIndex = 17;
            this.bJ.Text = "J";
            this.bJ.UseVisualStyleBackColor = true;
            this.bJ.Click += new System.EventHandler(this.bJ_Click);
            // 
            // bI
            // 
            this.bI.Enabled = false;
            this.bI.Location = new System.Drawing.Point(661, 318);
            this.bI.Name = "bI";
            this.bI.Size = new System.Drawing.Size(35, 29);
            this.bI.TabIndex = 16;
            this.bI.Text = "I";
            this.bI.UseVisualStyleBackColor = true;
            this.bI.Click += new System.EventHandler(this.bI_Click);
            // 
            // bO
            // 
            this.bO.Enabled = false;
            this.bO.Location = new System.Drawing.Point(718, 318);
            this.bO.Name = "bO";
            this.bO.Size = new System.Drawing.Size(35, 29);
            this.bO.TabIndex = 26;
            this.bO.Text = "O";
            this.bO.UseVisualStyleBackColor = true;
            this.bO.Click += new System.EventHandler(this.bO_Click);
            // 
            // bN
            // 
            this.bN.Enabled = false;
            this.bN.Location = new System.Drawing.Point(604, 388);
            this.bN.Name = "bN";
            this.bN.Size = new System.Drawing.Size(35, 29);
            this.bN.TabIndex = 25;
            this.bN.Text = "N";
            this.bN.UseVisualStyleBackColor = true;
            this.bN.Click += new System.EventHandler(this.bN_Click);
            // 
            // bM
            // 
            this.bM.Enabled = false;
            this.bM.Location = new System.Drawing.Point(661, 388);
            this.bM.Name = "bM";
            this.bM.Size = new System.Drawing.Size(35, 29);
            this.bM.TabIndex = 24;
            this.bM.Text = "M";
            this.bM.UseVisualStyleBackColor = true;
            this.bM.Click += new System.EventHandler(this.bM_Click);
            // 
            // bExit
            // 
            this.bExit.Location = new System.Drawing.Point(870, 402);
            this.bExit.Name = "bExit";
            this.bExit.Size = new System.Drawing.Size(189, 57);
            this.bExit.TabIndex = 27;
            this.bExit.Text = "Exit";
            this.bExit.UseVisualStyleBackColor = true;
            this.bExit.Click += new System.EventHandler(this.bExit_Click);
            // 
            // bNewGame
            // 
            this.bNewGame.Location = new System.Drawing.Point(870, 276);
            this.bNewGame.Name = "bNewGame";
            this.bNewGame.Size = new System.Drawing.Size(189, 57);
            this.bNewGame.TabIndex = 28;
            this.bNewGame.Text = "New Game";
            this.bNewGame.UseVisualStyleBackColor = true;
            this.bNewGame.Click += new System.EventHandler(this.bNewGame_Click);
            // 
            // lLetter
            // 
            this.lLetter.AutoSize = true;
            this.lLetter.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lLetter.Location = new System.Drawing.Point(15, 11);
            this.lLetter.Name = "lLetter";
            this.lLetter.Size = new System.Drawing.Size(0, 59);
            this.lLetter.TabIndex = 29;
            // 
            // lblNoOTries
            // 
            this.lblNoOTries.AutoSize = true;
            this.lblNoOTries.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblNoOTries.Location = new System.Drawing.Point(12, 388);
            this.lblNoOTries.Name = "lblNoOTries";
            this.lblNoOTries.Size = new System.Drawing.Size(151, 38);
            this.lblNoOTries.TabIndex = 30;
            this.lblNoOTries.Text = "Tries left:";
            // 
            // tbNoOTries
            // 
            this.tbNoOTries.BackColor = System.Drawing.SystemColors.Menu;
            this.tbNoOTries.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbNoOTries.Enabled = false;
            this.tbNoOTries.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tbNoOTries.Location = new System.Drawing.Point(155, 389);
            this.tbNoOTries.Name = "tbNoOTries";
            this.tbNoOTries.Size = new System.Drawing.Size(23, 37);
            this.tbNoOTries.TabIndex = 31;
            this.tbNoOTries.Text = "6";
            this.tbNoOTries.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // bReset
            // 
            this.bReset.Enabled = false;
            this.bReset.Location = new System.Drawing.Point(870, 339);
            this.bReset.Name = "bReset";
            this.bReset.Size = new System.Drawing.Size(189, 57);
            this.bReset.TabIndex = 32;
            this.bReset.Text = "Reset";
            this.bReset.UseVisualStyleBackColor = true;
            this.bReset.Click += new System.EventHandler(this.bReset_Click);
            // 
            // tTries
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1083, 471);
            this.Controls.Add(this.bReset);
            this.Controls.Add(this.tbNoOTries);
            this.Controls.Add(this.lblNoOTries);
            this.Controls.Add(this.lLetter);
            this.Controls.Add(this.bNewGame);
            this.Controls.Add(this.bExit);
            this.Controls.Add(this.bO);
            this.Controls.Add(this.bN);
            this.Controls.Add(this.bM);
            this.Controls.Add(this.bZ);
            this.Controls.Add(this.bY);
            this.Controls.Add(this.bX);
            this.Controls.Add(this.bL);
            this.Controls.Add(this.bK);
            this.Controls.Add(this.bJ);
            this.Controls.Add(this.bI);
            this.Controls.Add(this.bW);
            this.Controls.Add(this.bV);
            this.Controls.Add(this.bU);
            this.Controls.Add(this.bT);
            this.Controls.Add(this.bH);
            this.Controls.Add(this.bG);
            this.Controls.Add(this.bF);
            this.Controls.Add(this.bE);
            this.Controls.Add(this.bS);
            this.Controls.Add(this.bR);
            this.Controls.Add(this.bQ);
            this.Controls.Add(this.bP);
            this.Controls.Add(this.bD);
            this.Controls.Add(this.bC);
            this.Controls.Add(this.bB);
            this.Controls.Add(this.bA);
            this.Name = "tTries";
            this.Text = "Hanged Man";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bA;
        private System.Windows.Forms.Button bB;
        private System.Windows.Forms.Button bC;
        private System.Windows.Forms.Button bD;
        private System.Windows.Forms.Button bS;
        private System.Windows.Forms.Button bR;
        private System.Windows.Forms.Button bQ;
        private System.Windows.Forms.Button bP;
        private System.Windows.Forms.Button bW;
        private System.Windows.Forms.Button bV;
        private System.Windows.Forms.Button bU;
        private System.Windows.Forms.Button bT;
        private System.Windows.Forms.Button bH;
        private System.Windows.Forms.Button bG;
        private System.Windows.Forms.Button bF;
        private System.Windows.Forms.Button bE;
        private System.Windows.Forms.Button bZ;
        private System.Windows.Forms.Button bY;
        private System.Windows.Forms.Button bX;
        private System.Windows.Forms.Button bL;
        private System.Windows.Forms.Button bK;
        private System.Windows.Forms.Button bJ;
        private System.Windows.Forms.Button bI;
        private System.Windows.Forms.Button bO;
        private System.Windows.Forms.Button bN;
        private System.Windows.Forms.Button bM;
        private System.Windows.Forms.Button bExit;
        private System.Windows.Forms.Button bNewGame;
        private System.Windows.Forms.Label lLetter;
        private System.Windows.Forms.Label lblNoOTries;
        private System.Windows.Forms.TextBox tbNoOTries;
        private System.Windows.Forms.Button bReset;
    }
}

